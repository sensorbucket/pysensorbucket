"""The Python SensorBucket package simplifies interactions with the SensorBucket API.
The package makes it easier to fetch measurements and metadata
"""

from .facade import Facade
